# -*- coding: utf-8 -*-
import os
import subprocess

from Tkinter import *
import tkMessageBox
import tkFileDialog


class NXTpanel():
	def __init__(self):
		self.appInfo_sAppName = "NXTpanel"
		self.appInfo_sAppVersion = "v1.1"
		self.oTk = Tk()
		
		self.initMainWindow()
	# end of method __init__
	
	def exit(self):
		self.oTk.destroy()
	# end of method exit
	
	def checkFileNXC(self, sFilePath): 
		return os.path.isfile(sFilePath) and os.path.splitext(sFilePath)[1] == '.nxc'
	# end of method checkFileNXC
	
	def runNBC(self, bRun):
		sFilePath = self.inFilePathNXC_sFilePath.get()
		if not self.checkFileNXC(sFilePath):
			tkMessageBox.showerror("ERROR - File Selection", "Given path does not exist or the file is not .nxc file!")
			return
		
		sActionArg = "-d"
		if bRun:
			sActionArg = "-r"
		
		if self.compilerOptions_EnhancedFirmware.get():
			aNBCcall = ["nbc", "-S=usb", sActionArg, "-sm-", "-EF", sFilePath]
		else:
			aNBCcall = ["nbc", "-S=usb", sActionArg, "-sm-", sFilePath]
		
		while True:
			try:
				sNBCoutput = subprocess.check_output(aNBCcall, stderr=subprocess.STDOUT)
			except subprocess.CalledProcessError, e:
				if e.returncode == 2:
					if tkMessageBox.askretrycancel("ERROR - NBC", "     NXT Brick was not found!     \n\nPlease, connect it and then retry."):
						continue
				elif e.returncode == 1:
					tkMessageBox.showerror("ERROR - NBC", "Error in file syntax!" + "\n\n\n" + e.output)
					print e.output
				else:
					tkMessageBox.showerror("ERROR - NBC", "NBC RetCode: " + str(e.returncode) + "\n" + "'" + e.output + "'")
			break
	# end of method runNBC
	
	def runCompileDownload(self):
		self.runNBC(False)
	# end of method runCompileDownload
	
	def runCompileDownloadRun(self):
		self.runNBC(True)
	# end of method runCompileDownloadRun
	
	def getFileNXC(self):
		sFilePath = './'
		if self.checkFileNXC(self.inFilePathNXC_sFilePath.get()):
			sFilePath = self.inFilePathNXC_sFilePath.get()
		sFilePath = tkFileDialog.askopenfilename(defaultextension='.nxc', filetypes=[('NXC source', '.nxc')], initialdir=os.path.basename(sFilePath), initialfile=os.path.basename(sFilePath), parent=self.oTk, title='Choose NXC source code file')
		if os.path.isfile(sFilePath):
			self.inFilePathNXC_sFilePath.delete(0, END)
			self.inFilePathNXC_sFilePath.insert(0, sFilePath)
	# end of method getFileNXC
	
	def initMainWindow(self):
		self.oTk.wm_title(self.appInfo_sAppName+' '+self.appInfo_sAppVersion)
		self.oTk.geometry("+%d+%d" % (100, 300))
		
		configGroup_FilePathNXC = LabelFrame(self.oTk, text="NXC File Selection", font=("Helvetica", 12, "bold"), padx=5, pady=5)
		configGroup_FilePathNXC.grid(row=0, padx=10, pady=10)
		self.inFilePathNXC_sFilePath = Entry(configGroup_FilePathNXC, font=("Courier New", 11), width=50)
		self.inFilePathNXC_sFilePath.grid(row=0, column=0, padx=5, pady=2, sticky=W)
		Button(configGroup_FilePathNXC, text="Browse", font=("Helvetica", 11, "italic"), command=self.getFileNXC).grid(row=0, column=1, padx=5, pady=2, sticky=E)
		
		compilerOptions = LabelFrame(self.oTk, text="NBC Compiler Options", font=("Helvetica", 12, "bold"), padx=5, pady=5)
		compilerOptions.grid(row=1, padx=10, pady=10)
		self.compilerOptions_EnhancedFirmware = BooleanVar()
		Checkbutton(compilerOptions, text="Enhanced firmware (-EF)", variable=self.compilerOptions_EnhancedFirmware).grid(row=0, column=0, padx=10, pady=10)
		
		runButtons = LabelFrame(self.oTk, text="NXT Brick Actions", font=("Helvetica", 12, "bold"), padx=5, pady=5)
		runButtons.grid(row=2, padx=10, pady=10)
		Button(runButtons, text="Compile & Download", font=("Helvetica", 11, "bold italic"), command=self.runCompileDownload).grid(row=0, column=0, padx=10, pady=10, sticky=E)
		Button(runButtons, text="Compile & Download & Run", font=("Helvetica", 11, "bold italic"), command=self.runCompileDownloadRun).grid(row=0, column=1, padx=10, pady=10, sticky=W)
		
		Button(self.oTk, text="QUIT", font=("Helvetica", 11, "bold"), command=self.exit).grid(row=3, padx=10, pady=10)
		
		self.oTk.mainloop()
	# end of method initMainWindow
# end of class NXTpanel

NXTpanel()
